export interface iVisit {
    id: number;
    date: Date;
}
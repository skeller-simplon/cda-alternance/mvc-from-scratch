import VisitModel from "../model/VisitModel";

export default class VisitController {
    public model: VisitModel;
    constructor() {
        this.model = new VisitModel()
    }
    addVisit(date: Date) {
        if (date)
            this.model.createVisit(date)
    }
    getAllVisits() {
        return this.model.getAllVisits();
    }
}
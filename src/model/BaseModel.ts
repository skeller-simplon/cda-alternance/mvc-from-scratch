import ModelPersistence from "./ModelPersistence";

export default class BaseModel<T> extends ModelPersistence<T>{

    constructor(entityName: string) {
        super(entityName)
    }
    /**
     * Récupères à partir d'un tableau la plus grand id utilisée et renvoie l'id suivante.
     * Les ids commencent à 1 (0++).
     */
    fetchNextId(): number {
        let id = 0
        this.getAll().map(
            item => {
                if (item[id] > id)
                    id = item[id]
            })
        return id++;
    }
}
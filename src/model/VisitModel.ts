import type { iVisit } from "src/lib/iVisit";
import BaseModel from "./BaseModel";

export default class VisitModel extends BaseModel<iVisit> {
    constructor() {
        super("visit")
    }
    createVisit(date: Date): void {
        this.add({ id: this.fetchNextId(), date })
    }
    getAllVisits() {
        return this.getAll()
    }

}
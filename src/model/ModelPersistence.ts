/**
 * Gère l'écriture des données en localstorage.
 * Cette classe représente les models de chaque entité, stockés graçe à la localStorageKey.
 */
export default class ModelPersistence<T> {
    protected localStorageKey = "";

    constructor(entityName: string) {
        this.localStorageKey = entityName;
        localStorage.setItem(this.localStorageKey, "[]")
    }
    protected add(data: T): void {
        let savedArray: Array<T> = this.getAll()
        savedArray.push(data)
        localStorage.setItem(this.localStorageKey, JSON.stringify(savedArray))
    }
    protected getAll(): Array<T> {
        return JSON.parse(localStorage.getItem(this.localStorageKey));
    }
}
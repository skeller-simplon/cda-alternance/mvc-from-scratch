import VisitController from "./controller/VisitController";


console.log("[js] - index.ts loaded");

let visitController = new VisitController();
(<any>window).visitController = visitController;


function renderVisitsList() {
    const visits = visitController.getAllVisits()
    const visitListParent = document.getElementById("visit-list");
    for (const visit of visits) {

        const card = document.createElement("div");
        card.className = "card"
        const cardHeader = document.createElement("div");
        cardHeader.className = "card-header";
        const cardContent = document.createElement("div").className = "card-content"


        // <div class="card">
        // <header class="card-header">
        //   <p class="card-header-title">
        // Component
        //   </p>
        // </header>

        //     <div class="card-content">
        //       Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus nec iaculis mauris.
        //       <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
        //   </div>
        // </div>

        visitListParent.appendChild(card)
    }
}
